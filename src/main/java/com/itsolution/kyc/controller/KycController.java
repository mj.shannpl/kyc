package com.itsolution.kyc.controller;

import com.itsolution.kyc.dto.KycDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class KycController {

    @RequestMapping("/kyc")
    public String kyc(Model model)
    {
        model.addAttribute("kycDto", new KycDto());
        return "kyc";
    }

    @RequestMapping("/map")
    public String googleMap()
    {
        return "google_map";
    }

    @RequestMapping(value="/kycForm", method = RequestMethod.POST)
    public String kycSubmit(@ModelAttribute KycDto kycDto, BindingResult bindingResult){
        return "kycResult";
    }

}
