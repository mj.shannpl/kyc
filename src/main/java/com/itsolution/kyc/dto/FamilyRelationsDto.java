package com.itsolution.kyc.dto;

import lombok.Data;

import com.itsolution.kyc.constants.Relationship;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Data
public class FamilyRelationsDto {

    private String fullName;
    private String contactNo;
    private String citizenshipNo;
    private Relationship relationship;

}
