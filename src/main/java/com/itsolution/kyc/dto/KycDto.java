package com.itsolution.kyc.dto;

import java.sql.Date;
import java.util.List;

import lombok.Data;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Data
public class KycDto {
    private String accountNumber;
    private String cardNumber;
    private DocumentDto documentDto;
    private PersonalDetailDto personalDetailDto;
    private List<FamilyRelationsDto> familyRelations;
    private List<BusinessDto> businesses;
}
