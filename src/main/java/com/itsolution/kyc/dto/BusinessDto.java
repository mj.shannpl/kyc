package com.itsolution.kyc.dto;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
public class BusinessDto {
    private String name;
    private String address;
    private String contactNo;
    private String designation;
    private Double yearlyIncome;

}
