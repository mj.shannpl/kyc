package com.itsolution.kyc.dto;

import lombok.Data;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Data
public class PersonalDetailDto {

    private String name;
    private String citizenshipNo;
    private String citizenIssuingAuthority;
    private String citizenIssuedDate;
    private Boolean passportObtainedFlag;
    private String passportNo;
    private String passportIssuingAuthority;
    private String passoprtIssuedDate;
    private String residenceContactNo;
    private String dob;
    private String email;
    private String mobileNo;
    private String poBoxNo;
    private String panNo;
    private AddressDto presentAddress;
    private AddressDto permanentAddress;

}
