package com.itsolution.kyc.constants;

import lombok.experimental.UtilityClass;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@UtilityClass
public class AppConstant {

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
