package com.itsolution.kyc.constants;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
public enum Relationship {
    SPOUSE, FATHER, MOTHER, GRAND_FATHER, GRAND_MOTHER, SON, DAUGHTER, DAUGHTER_IN_LAW, FATHER_IN_LAW
}
