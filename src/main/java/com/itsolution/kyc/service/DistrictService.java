package com.itsolution.kyc.service;

import com.itsolution.kyc.entity.District;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
public interface DistrictService extends BaseService<District> {

}
