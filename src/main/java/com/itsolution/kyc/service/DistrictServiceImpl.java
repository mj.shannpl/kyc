package com.itsolution.kyc.service;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.itsolution.kyc.entity.District;
import com.itsolution.kyc.respository.DistrictRepository;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@RequiredArgsConstructor
public class DistrictServiceImpl implements DistrictService {

    private final DistrictRepository districtRepository;

    @Override
    public List<District> findAll() {
        return districtRepository.findAll();
    }

    @Override
    public Page<District> findPaginatedPage(Pageable pageable) {
        return districtRepository.findAll(pageable);
    }

    @Override
    public District findById(Long id) {
        return districtRepository.getOne(id);
    }
}
